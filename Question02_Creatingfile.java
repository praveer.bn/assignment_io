package week5.Assignment_FileIO;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Question02_Creatingfile {
    public static void main(String[] args) throws IOException {
        FileReader fileReader1=new FileReader("C:\\Users\\coditas\\IdeaProjects\\java\\src\\week5\\Assignment_FileIO\\Question02_Employee_Scanner.java");
        FileReader fileReader2=new FileReader("C:\\Users\\coditas\\IdeaProjects\\java\\src\\week5\\Assignment_FileIO\\Question02_Employee_BufferdReader.java");
        FileReader fileReader3=new FileReader("C:\\Users\\coditas\\IdeaProjects\\java\\src\\week5\\Assignment_FileIO\\Question02_Employee_CLI.java");
        FileWriter fileWriter1=new FileWriter("Scanner.txt");
        FileWriter fileWriter2=new FileWriter("BR.txt");
        FileWriter fileWriter3=new FileWriter("CLI.txt");

        int i;
        while ((i=fileReader1.read())!= -1){
            fileWriter1.write((char)i);
        }
        fileWriter1.close();
        fileReader1.close();
        int j;
        while ((j=fileReader2.read())!= -1){
            fileWriter2.write((char)j);
        }
        fileWriter2.close();
        fileReader2.close();
        int k;
        while ((k=fileReader3.read())!= -1){
            fileWriter3.write((char)k);
        }
        fileWriter3.close();
        fileReader3.close();
        System.out.println("done");
    }
}
