package week5.Assignment_FileIO;

import java.util.Scanner;

public class Question02_Employee_Scanner {
    static int id;
    static int salary;
    static String name;
    static void acceptDetailsUsingScanner(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the id");
        id=scanner.nextInt();
        scanner.nextLine();


        System.out.println("Enter the name");
        name=scanner.nextLine();


        System.out.println("Enter the salary");
        salary=scanner.nextInt();

    }

    static void showDetails(){
        System.out.println("the details of Employee is:: ");
        System.out.println("The id is :"+id);
        System.out.println("The name is :"+name);
        System.out.println("The percentage is :"+salary);
    }

    public static void main(String[] args) {
        acceptDetailsUsingScanner();
        showDetails();
    }
}
