package week5.Assignment_FileIO;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Question05_CloseMEthode {
        public static void main(String[] args) throws IOException {
            FileWriter fileWriter=new FileWriter("hello1.txt");
            PrintWriter pw=new PrintWriter(fileWriter);
            pw.print("hello");
            pw.print("Madara Uchiha");
            pw.append("hello");
            pw.close();
            fileWriter.close();
        }
    }


//if i did't close the file ,then the file is not updating ..so cloe methode if Important