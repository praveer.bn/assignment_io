package week5.Assignment_FileIO;
import java.io.*;
public class Question06_Password {

        public static void main(String args[]) {
            try {
                String logFileName = args[0];
                File f = new File(logFileName);
                boolean access_granted = false;
                String password = "";
                int integer = 5;

                if (args.length == 2) {
                    System.out.println("Checking command-line password");
                    password = password + args[1];
                    if (password.equals("3dTAqb.7")) {
                        access_granted = true;
                        System.out.println("Password matches.");
                    } else
                        System.out.println("Command-line password does not match");
                }//end if

                if (access_granted) {
                    System.out.println("Access granted!");
                    System.out.println("Password is :: "+password);

                    PrintWriter out = new PrintWriter(new FileOutputStream(f, true));
                    FileInputStream f1 = new FileInputStream( args[0]);


                    out.println();
                    out.print("Updated...");
                    out.println();

                    DataInputStream din = new DataInputStream(f1);
                    int i ;
                    while ((i = din.read()) != -1) {
                        System.out.print((char) i);
                    }
                    f1.close();
                    out.flush();
                    out.close();
                }//end if


            }//end try
            catch (Exception e) {
                System.out.println("an error has occured.");
                //e.printStackTrace();
            }


        }//end main
    }//end class

/*

Checking command-line password
Password matches.
Access granted!
Password is :: 3dTAqb.7

* */

