package week5.Assignment_FileIO;

import java.io.*;

public class Question04_Printwriter {
    public static void main(String[] args) throws IOException {
        FileWriter fileWriter=new FileWriter("hello1.txt");
        PrintWriter pw=new PrintWriter(fileWriter);
        pw.print("hello");
        pw.print("Madara Uchiha");
        pw.append("hello");
        pw.close();
        fileWriter.close();
    }
}
