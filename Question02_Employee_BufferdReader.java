package week5.Assignment_FileIO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Question02_Employee_BufferdReader {
    static int id;
    static int salary;
    static String name;
    static void acceptDetailsUsingBufferdReader() throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the name");
        name=br.readLine();

        System.out.println("Enter the id");
        id=Integer.parseInt(br.readLine());

        System.out.println("Enter the salary");
        salary=Integer.parseInt(br.readLine());
    }
    static void showDetails(){
        System.out.println("the details of Employee is:: ");
        System.out.println("The id is :"+id);
        System.out.println("The name is :"+name);
        System.out.println("The percentage is :"+salary);
    }

    public static void main(String[] args) throws IOException {
        acceptDetailsUsingBufferdReader();
        showDetails();
    }
}
